import { NgModule } from '@angular/core';
import { Storage } from '@ionic/storage';
import { IonicApp, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { StartPage } from '../pages/start/start';
import { LoginPage } from '../pages/login/login';
import { RegistrationPage } from '../pages/registration/registration';
import { MapComponent } from '../components/map/map';
import { ModalAutoCompleteComponent } from '../components/modal-auto-complete/modal-auto-complete';
import { AddAdsPage } from '../pages/add-ads/add-ads';
import { ApplicantsPage } from '../pages/applicants/applicants';
import { AdsdetailPage } from '../pages/adsdetail/adsdetail';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { OrdersPage } from '../pages/orders/orders';
import { Cartpage } from '../pages/cartpage/cartpage';
import { PopcoverpagePage } from '../pages/popcoverpage/popcoverpage';
import { ViewAdsPage } from '../pages/view-ads/view-ads';
import { EditAdsPage } from '../pages/edit-ads/edit-ads';
import { Config } from '../providers/config';
import { Userdata } from '../providers/userdata';
import { AdvertisementService } from '../providers/advertisement';
export var AppModule = (function () {
    function AppModule() {
    }
    AppModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        MyApp,
                        AboutPage,
                        ContactPage,
                        HomePage,
                        StartPage,
                        LoginPage,
                        RegistrationPage,
                        MapComponent,
                        ModalAutoCompleteComponent,
                        AddAdsPage,
                        ApplicantsPage,
                        AdsdetailPage,
                        MyaccountPage,
                        OrdersPage,
                        Cartpage,
                        PopcoverpagePage,
                        ViewAdsPage,
                        EditAdsPage
                    ],
                    imports: [
                        IonicModule.forRoot(MyApp)
                    ],
                    bootstrap: [IonicApp],
                    entryComponents: [
                        MyApp,
                        AboutPage,
                        ContactPage,
                        HomePage,
                        StartPage,
                        LoginPage,
                        RegistrationPage,
                        MapComponent,
                        ModalAutoCompleteComponent,
                        AddAdsPage,
                        ApplicantsPage,
                        AdsdetailPage,
                        MyaccountPage,
                        OrdersPage,
                        Cartpage,
                        PopcoverpagePage,
                        ViewAdsPage,
                        EditAdsPage
                    ],
                    providers: [Config, Userdata, AdvertisementService, Storage]
                },] },
    ];
    /** @nocollapse */
    AppModule.ctorParameters = [];
    return AppModule;
}());
//# sourceMappingURL=app.module.js.map