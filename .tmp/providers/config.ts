import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


/*
  Generated class for the Config provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Config {

  public API_URL: string;

  constructor() {
   this.API_URL = 'http://ec2-35-154-15-127.ap-south-1.compute.amazonaws.com:3000';
   //this.API_URL = 'http://localhost:3000';
  }
}
