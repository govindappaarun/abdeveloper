import { Injectable } from '@angular/core';
import { Events , AlertController} from 'ionic-angular';
import { Http} from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';

import {Config} from './config';


/*
  Generated class for the Userdata provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class Userdata {
  public API_URL: string;
  
  HAS_LOGGED_IN = 'hasLoggedIn';
  ROLE = 'buyer';

  constructor(public http: Http, public config: Config, public events: Events, public storage: Storage, public alertCtrl: AlertController) {
    console.log('Hello Userdata Provider');
    this.API_URL = config.API_URL;    
  }

  login(user) {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(user.email);
    window.localStorage.setItem('user', JSON.stringify(user));
    this.events.publish('user:login', user);
  };

  signup(user) {
    this.storage.set(this.HAS_LOGGED_IN, true);
    this.setUsername(user.email);
    window.localStorage.setItem('user', JSON.stringify(user));
    this.events.publish('user:signup', user);
  };

  logout() {
    this.storage.remove(this.HAS_LOGGED_IN);
    this.storage.remove('username');
    this.events.publish('user:logout');
  };

   hasLoggedIn() {
    return this.storage.get(this.HAS_LOGGED_IN).then((value) => {
      return value === true;
    });
  };
  
  getUsername() {
    return this.storage.get('username').then((value) => {
      return value;
    });
  };

  setUsername(username) {
    this.storage.set('username', username);
  };

}
