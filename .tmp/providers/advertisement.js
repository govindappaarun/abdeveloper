import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import { Config } from './config';
/*
  Generated class for the Advertisement provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export var AdvertisementService = (function () {
    function AdvertisementService(http, config, alertCtrl, loadingCtrl) {
        this.http = http;
        this.config = config;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        console.log('Hello Advertisement Provider');
        this.API_URL = config.API_URL;
    }
    AdvertisementService.prototype.getAll = function () {
        var _this = this;
        var data = Observable.create(function (observable) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            _this.http.get(_this.API_URL + '/api/advertisements', { headers: headers })
                .subscribe(function (data) {
                var res = data.json();
                observable.next(res.data);
                // loadingPopup.dismiss();
            }, function (err) {
                var res = err.json();
                observable.next(res.data);
                //loadingPopup.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Advertisements Not found',
                    subTitle: res.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        });
        return data;
    };
    ;
    AdvertisementService.prototype.getAdsByOption = function (options) {
        //Create the popup
        /* let loadingPopup = this.loadingCtrl.create({
           content: 'Your Location...'
         });
     
         // Show the popup
         loadingPopup.present();*/
        var _this = this;
        var data = Observable.create(function (observable) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            _this.http.post(_this.API_URL + '/api/AdvertisementsBy', options, { headers: headers })
                .subscribe(function (data) {
                var res = data.json();
                observable.next(res.data);
                // loadingPopup.dismiss();
            }, function (err) {
                var res = err.json();
                observable.next(res.data);
                //loadingPopup.dismiss();
                var alert = _this.alertCtrl.create({
                    title: 'Advertisements Not found',
                    subTitle: res.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        });
        return data;
    };
    ;
    AdvertisementService.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    AdvertisementService.ctorParameters = [
        { type: Http, },
        { type: Config, },
        { type: AlertController, },
        { type: LoadingController, },
    ];
    return AdvertisementService;
}());
//# sourceMappingURL=advertisement.js.map