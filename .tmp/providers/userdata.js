import { Injectable } from '@angular/core';
import { Events, AlertController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import { Config } from './config';
/*
  Generated class for the Userdata provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
export var Userdata = (function () {
    function Userdata(http, config, events, storage, alertCtrl) {
        this.http = http;
        this.config = config;
        this.events = events;
        this.storage = storage;
        this.alertCtrl = alertCtrl;
        this.HAS_LOGGED_IN = 'hasLoggedIn';
        this.ROLE = 'buyer';
        console.log('Hello Userdata Provider');
        this.API_URL = config.API_URL;
    }
    Userdata.prototype.login = function (user) {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(user.email);
        window.localStorage.setItem('user', JSON.stringify(user));
        this.events.publish('user:login', user);
    };
    ;
    Userdata.prototype.signup = function (user) {
        this.storage.set(this.HAS_LOGGED_IN, true);
        this.setUsername(user.email);
        window.localStorage.setItem('user', JSON.stringify(user));
        this.events.publish('user:signup', user);
    };
    ;
    Userdata.prototype.logout = function () {
        this.storage.remove(this.HAS_LOGGED_IN);
        this.storage.remove('username');
        this.events.publish('user:logout');
    };
    ;
    Userdata.prototype.hasLoggedIn = function () {
        return this.storage.get(this.HAS_LOGGED_IN).then(function (value) {
            return value === true;
        });
    };
    ;
    Userdata.prototype.getUsername = function () {
        return this.storage.get('username').then(function (value) {
            return value;
        });
    };
    ;
    Userdata.prototype.setUsername = function (username) {
        this.storage.set('username', username);
    };
    ;
    Userdata.decorators = [
        { type: Injectable },
    ];
    /** @nocollapse */
    Userdata.ctorParameters = [
        { type: Http, },
        { type: Config, },
        { type: Events, },
        { type: Storage, },
        { type: AlertController, },
    ];
    return Userdata;
}());
//# sourceMappingURL=userdata.js.map