import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';
import { LoginPage } from '../login/login';
import { RegistrationPage } from '../registration/registration';
/*
  Generated class for the Start page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var StartPage = (function () {
    function StartPage(navCtrl, menu) {
        this.navCtrl = navCtrl;
        this.menu = menu;
    }
    StartPage.prototype.ionViewDidLoad = function () {
        console.log('Hello StartPage Page');
    };
    StartPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(false);
    };
    StartPage.prototype.ionViewCanLeave = function () {
        this.menu.enable(true);
    };
    StartPage.prototype.launchLogin = function () {
        this.navCtrl.push(LoginPage);
    };
    StartPage.prototype.launchRegistration = function () {
        this.navCtrl.push(RegistrationPage);
    };
    StartPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-start',
                    templateUrl: 'start.html'
                },] },
    ];
    /** @nocollapse */
    StartPage.ctorParameters = [
        { type: NavController, },
        { type: MenuController, },
    ];
    return StartPage;
}());
//# sourceMappingURL=start.js.map