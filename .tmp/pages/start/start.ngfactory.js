/**
 * This file is generated by the Angular 2 template compiler.
 * Do not edit.
 */
/* tslint:disable */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import * as import0 from './start';
import * as import1 from '@angular/core/src/linker/view';
import * as import3 from '@angular/core/src/linker/element';
import * as import4 from '@angular/core/src/linker/view_utils';
import * as import6 from '@angular/core/src/linker/view_type';
import * as import7 from '@angular/core/src/change_detection/change_detection';
import * as import8 from 'ionic-angular/navigation/nav-controller';
import * as import9 from 'ionic-angular/components/menu/menu-controller';
import * as import10 from '@angular/core/src/metadata/view';
import * as import11 from '@angular/core/src/linker/component_factory';
import * as import12 from '../../node_modules/ionic-angular/components/toolbar/toolbar.ngfactory';
import * as import13 from '../../node_modules/ionic-angular/components/content/content.ngfactory';
import * as import14 from '../../node_modules/ionic-angular/components/grid/grid.ngfactory';
import * as import15 from '../../node_modules/ionic-angular/components/button/button.ngfactory';
import * as import16 from 'ionic-angular/config/config';
import * as import17 from '@angular/core/src/linker/element_ref';
import * as import18 from 'ionic-angular/navigation/view-controller';
import * as import19 from 'ionic-angular/components/app/app';
import * as import20 from 'ionic-angular/util/keyboard';
import * as import21 from '@angular/core/src/zone/ng_zone';
import * as import22 from 'ionic-angular/components/tabs/tabs';
import * as import23 from 'ionic-angular/components/toolbar/toolbar';
import * as import24 from 'ionic-angular/components/content/content';
import * as import25 from 'ionic-angular/components/button/button';
import * as import26 from 'ionic-angular/components/grid/grid';
export var Wrapper_StartPage = (function () {
    function Wrapper_StartPage(p0, p1) {
        this.changed = false;
        this.context = new import0.StartPage(p0, p1);
    }
    Wrapper_StartPage.prototype.detectChangesInternal = function (view, el, throwOnChange) {
        var changed = this.changed;
        this.changed = false;
        return changed;
    };
    return Wrapper_StartPage;
}());
var renderType_StartPage_Host = null;
var _View_StartPage_Host0 = (function (_super) {
    __extends(_View_StartPage_Host0, _super);
    function _View_StartPage_Host0(viewUtils, parentInjector, declarationEl) {
        _super.call(this, _View_StartPage_Host0, renderType_StartPage_Host, import6.ViewType.HOST, viewUtils, parentInjector, declarationEl, import7.ChangeDetectorStatus.CheckAlways);
    }
    _View_StartPage_Host0.prototype.createInternal = function (rootSelector) {
        this._el_0 = this.selectOrCreateHostElement('page-start', rootSelector, null);
        this._appEl_0 = new import3.AppElement(0, null, this, this._el_0);
        var compView_0 = viewFactory_StartPage0(this.viewUtils, this.injector(0), this._appEl_0);
        this._StartPage_0_4 = new Wrapper_StartPage(this.parentInjector.get(import8.NavController), this.parentInjector.get(import9.MenuController));
        this._appEl_0.initComponent(this._StartPage_0_4.context, [], compView_0);
        compView_0.create(this._StartPage_0_4.context, this.projectableNodes, null);
        this.init([].concat([this._el_0]), [this._el_0], [], []);
        return this._appEl_0;
    };
    _View_StartPage_Host0.prototype.injectorGetInternal = function (token, requestNodeIndex, notFoundResult) {
        if (((token === import0.StartPage) && (0 === requestNodeIndex))) {
            return this._StartPage_0_4.context;
        }
        return notFoundResult;
    };
    _View_StartPage_Host0.prototype.detectChangesInternal = function (throwOnChange) {
        this._StartPage_0_4.detectChangesInternal(this, this._el_0, throwOnChange);
        this.detectContentChildrenChanges(throwOnChange);
        this.detectViewChildrenChanges(throwOnChange);
    };
    return _View_StartPage_Host0;
}(import1.AppView));
function viewFactory_StartPage_Host0(viewUtils, parentInjector, declarationEl) {
    if ((renderType_StartPage_Host === null)) {
        (renderType_StartPage_Host = viewUtils.createRenderComponentType('', 0, import10.ViewEncapsulation.None, [], {}));
    }
    return new _View_StartPage_Host0(viewUtils, parentInjector, declarationEl);
}
export var StartPageNgFactory = new import11.ComponentFactory('page-start', viewFactory_StartPage_Host0, import0.StartPage);
var styles_StartPage = [];
var renderType_StartPage = null;
var _View_StartPage0 = (function (_super) {
    __extends(_View_StartPage0, _super);
    function _View_StartPage0(viewUtils, parentInjector, declarationEl) {
        _super.call(this, _View_StartPage0, renderType_StartPage, import6.ViewType.COMPONENT, viewUtils, parentInjector, declarationEl, import7.ChangeDetectorStatus.CheckAlways);
    }
    _View_StartPage0.prototype.createInternal = function (rootSelector) {
        var parentRenderNode = this.renderer.createViewRoot(this.declarationAppElement.nativeElement);
        this._text_0 = this.renderer.createText(parentRenderNode, '\n', null);
        this._el_1 = this.renderer.createElement(parentRenderNode, 'ion-header', null);
        this._Header_1_3 = new import12.Wrapper_Header(this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_1), this.renderer, this.parentInjector.get(import18.ViewController, null));
        this._text_2 = this.renderer.createText(this._el_1, '\n\n ', null);
        this._text_3 = this.renderer.createText(this._el_1, '\n\n', null);
        this._text_4 = this.renderer.createText(parentRenderNode, '\n\n\n', null);
        this._el_5 = this.renderer.createElement(parentRenderNode, 'ion-content', null);
        this.renderer.setElementAttribute(this._el_5, 'padding', '');
        this._appEl_5 = new import3.AppElement(5, null, this, this._el_5);
        var compView_5 = import13.viewFactory_Content0(this.viewUtils, this.injector(5), this._appEl_5);
        this._Content_5_4 = new import13.Wrapper_Content(this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_5), this.renderer, this.parentInjector.get(import19.App), this.parentInjector.get(import20.Keyboard), this.parentInjector.get(import21.NgZone), this.parentInjector.get(import18.ViewController, null), this.parentInjector.get(import22.Tabs, null));
        this._appEl_5.initComponent(this._Content_5_4.context, [], compView_5);
        this._text_6 = this.renderer.createText(null, '\n\n', null);
        compView_5.create(this._Content_5_4.context, [
            [],
            [].concat([this._text_6]),
            []
        ], null);
        this._text_7 = this.renderer.createText(parentRenderNode, '\n', null);
        this._el_8 = this.renderer.createElement(parentRenderNode, 'ion-footer', null);
        this._Footer_8_3 = new import12.Wrapper_Footer(this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_8), this.renderer, this.parentInjector.get(import18.ViewController, null));
        this._text_9 = this.renderer.createText(this._el_8, '\n  ', null);
        this._el_10 = this.renderer.createElement(this._el_8, 'ion-toolbar', null);
        this.renderer.setElementAttribute(this._el_10, 'class', 'toolbar');
        this._appEl_10 = new import3.AppElement(10, 8, this, this._el_10);
        var compView_10 = import12.viewFactory_Toolbar0(this.viewUtils, this.injector(10), this._appEl_10);
        this._Toolbar_10_4 = new import12.Wrapper_Toolbar(this.parentInjector.get(import18.ViewController, null), this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_10), this.renderer);
        this._appEl_10.initComponent(this._Toolbar_10_4.context, [], compView_10);
        this._text_11 = this.renderer.createText(null, '\n    ', null);
        this._el_12 = this.renderer.createElement(null, 'ion-row', null);
        this._Row_12_3 = new import14.Wrapper_Row();
        this._text_13 = this.renderer.createText(this._el_12, '\n      ', null);
        this._el_14 = this.renderer.createElement(this._el_12, 'ion-col', null);
        this.renderer.setElementAttribute(this._el_14, 'width', '50');
        this._Col_14_3 = new import14.Wrapper_Col();
        this._text_15 = this.renderer.createText(this._el_14, '\n        ', null);
        this._el_16 = this.renderer.createElement(this._el_14, 'button', null);
        this.renderer.setElementAttribute(this._el_16, 'full', '');
        this.renderer.setElementAttribute(this._el_16, 'ion-button', '');
        this._appEl_16 = new import3.AppElement(16, 14, this, this._el_16);
        var compView_16 = import15.viewFactory_Button0(this.viewUtils, this.injector(16), this._appEl_16);
        this._Button_16_4 = new import15.Wrapper_Button(null, '', this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_16), this.renderer);
        this._appEl_16.initComponent(this._Button_16_4.context, [], compView_16);
        this._text_17 = this.renderer.createText(null, 'Login', null);
        compView_16.create(this._Button_16_4.context, [[].concat([this._text_17])], null);
        this._text_18 = this.renderer.createText(this._el_14, '\n      ', null);
        this._text_19 = this.renderer.createText(this._el_12, '\n      ', null);
        this._el_20 = this.renderer.createElement(this._el_12, 'ion-col', null);
        this.renderer.setElementAttribute(this._el_20, 'width', '50');
        this._Col_20_3 = new import14.Wrapper_Col();
        this._text_21 = this.renderer.createText(this._el_20, '\n        ', null);
        this._el_22 = this.renderer.createElement(this._el_20, 'button', null);
        this.renderer.setElementAttribute(this._el_22, 'full', '');
        this.renderer.setElementAttribute(this._el_22, 'ion-button', '');
        this._appEl_22 = new import3.AppElement(22, 20, this, this._el_22);
        var compView_22 = import15.viewFactory_Button0(this.viewUtils, this.injector(22), this._appEl_22);
        this._Button_22_4 = new import15.Wrapper_Button(null, '', this.parentInjector.get(import16.Config), new import17.ElementRef(this._el_22), this.renderer);
        this._appEl_22.initComponent(this._Button_22_4.context, [], compView_22);
        this._text_23 = this.renderer.createText(null, 'Register', null);
        compView_22.create(this._Button_22_4.context, [[].concat([this._text_23])], null);
        this._text_24 = this.renderer.createText(this._el_20, '\n      ', null);
        this._text_25 = this.renderer.createText(this._el_12, '\n    ', null);
        this._text_26 = this.renderer.createText(null, '\n  ', null);
        compView_10.create(this._Toolbar_10_4.context, [
            [],
            [],
            [],
            [].concat([
                this._text_11,
                this._el_12,
                this._text_26
            ])
        ], null);
        this._text_27 = this.renderer.createText(this._el_8, '\n', null);
        this._expr_0 = import7.UNINITIALIZED;
        this._expr_1 = import7.UNINITIALIZED;
        var disposable_0 = this.renderer.listen(this._el_16, 'click', this.eventHandler(this._handle_click_16_0.bind(this)));
        var disposable_1 = this.renderer.listen(this._el_22, 'click', this.eventHandler(this._handle_click_22_0.bind(this)));
        this.init([], [
            this._text_0,
            this._el_1,
            this._text_2,
            this._text_3,
            this._text_4,
            this._el_5,
            this._text_6,
            this._text_7,
            this._el_8,
            this._text_9,
            this._el_10,
            this._text_11,
            this._el_12,
            this._text_13,
            this._el_14,
            this._text_15,
            this._el_16,
            this._text_17,
            this._text_18,
            this._text_19,
            this._el_20,
            this._text_21,
            this._el_22,
            this._text_23,
            this._text_24,
            this._text_25,
            this._text_26,
            this._text_27
        ], [
            disposable_0,
            disposable_1
        ], []);
        return null;
    };
    _View_StartPage0.prototype.injectorGetInternal = function (token, requestNodeIndex, notFoundResult) {
        if (((token === import23.Header) && ((1 <= requestNodeIndex) && (requestNodeIndex <= 3)))) {
            return this._Header_1_3.context;
        }
        if (((token === import24.Content) && ((5 <= requestNodeIndex) && (requestNodeIndex <= 6)))) {
            return this._Content_5_4.context;
        }
        if (((token === import25.Button) && ((16 <= requestNodeIndex) && (requestNodeIndex <= 17)))) {
            return this._Button_16_4.context;
        }
        if (((token === import26.Col) && ((14 <= requestNodeIndex) && (requestNodeIndex <= 18)))) {
            return this._Col_14_3.context;
        }
        if (((token === import25.Button) && ((22 <= requestNodeIndex) && (requestNodeIndex <= 23)))) {
            return this._Button_22_4.context;
        }
        if (((token === import26.Col) && ((20 <= requestNodeIndex) && (requestNodeIndex <= 24)))) {
            return this._Col_20_3.context;
        }
        if (((token === import26.Row) && ((12 <= requestNodeIndex) && (requestNodeIndex <= 25)))) {
            return this._Row_12_3.context;
        }
        if (((token === import23.Toolbar) && ((10 <= requestNodeIndex) && (requestNodeIndex <= 26)))) {
            return this._Toolbar_10_4.context;
        }
        if (((token === import23.Footer) && ((8 <= requestNodeIndex) && (requestNodeIndex <= 27)))) {
            return this._Footer_8_3.context;
        }
        return notFoundResult;
    };
    _View_StartPage0.prototype.detectChangesInternal = function (throwOnChange) {
        this._Header_1_3.detectChangesInternal(this, this._el_1, throwOnChange);
        if (this._Content_5_4.detectChangesInternal(this, this._el_5, throwOnChange)) {
            this._appEl_5.componentView.markAsCheckOnce();
        }
        this._Footer_8_3.detectChangesInternal(this, this._el_8, throwOnChange);
        if (this._Toolbar_10_4.detectChangesInternal(this, this._el_10, throwOnChange)) {
            this._appEl_10.componentView.markAsCheckOnce();
        }
        this._Row_12_3.detectChangesInternal(this, this._el_12, throwOnChange);
        this._Col_14_3.detectChangesInternal(this, this._el_14, throwOnChange);
        var currVal_3 = '';
        this._Button_16_4.check_full(currVal_3, throwOnChange, false);
        if (this._Button_16_4.detectChangesInternal(this, this._el_16, throwOnChange)) {
            this._appEl_16.componentView.markAsCheckOnce();
        }
        this._Col_20_3.detectChangesInternal(this, this._el_20, throwOnChange);
        var currVal_5 = '';
        this._Button_22_4.check_full(currVal_5, throwOnChange, false);
        if (this._Button_22_4.detectChangesInternal(this, this._el_22, throwOnChange)) {
            this._appEl_22.componentView.markAsCheckOnce();
        }
        this.detectContentChildrenChanges(throwOnChange);
        if (!throwOnChange) {
            if ((this.numberOfChecks === 0)) {
                this._Button_16_4.context.ngAfterContentInit();
            }
            if ((this.numberOfChecks === 0)) {
                this._Button_22_4.context.ngAfterContentInit();
            }
        }
        var currVal_0 = this._Content_5_4.context._sbPadding;
        if (import4.checkBinding(throwOnChange, this._expr_0, currVal_0)) {
            this.renderer.setElementClass(this._el_5, 'statusbar-padding', currVal_0);
            this._expr_0 = currVal_0;
        }
        var currVal_1 = this._Toolbar_10_4.context._sbPadding;
        if (import4.checkBinding(throwOnChange, this._expr_1, currVal_1)) {
            this.renderer.setElementClass(this._el_10, 'statusbar-padding', currVal_1);
            this._expr_1 = currVal_1;
        }
        this.detectViewChildrenChanges(throwOnChange);
    };
    _View_StartPage0.prototype.destroyInternal = function () {
        this._Content_5_4.context.ngOnDestroy();
    };
    _View_StartPage0.prototype._handle_click_16_0 = function ($event) {
        this.markPathToRootAsCheckOnce();
        var pd_0 = (this.context.launchLogin() !== false);
        return (true && pd_0);
    };
    _View_StartPage0.prototype._handle_click_22_0 = function ($event) {
        this.markPathToRootAsCheckOnce();
        var pd_0 = (this.context.launchRegistration() !== false);
        return (true && pd_0);
    };
    return _View_StartPage0;
}(import1.AppView));
export function viewFactory_StartPage0(viewUtils, parentInjector, declarationEl) {
    if ((renderType_StartPage === null)) {
        (renderType_StartPage = viewUtils.createRenderComponentType('', 0, import10.ViewEncapsulation.None, styles_StartPage, {}));
    }
    return new _View_StartPage0(viewUtils, parentInjector, declarationEl);
}
//# sourceMappingURL=start.ngfactory.js.map