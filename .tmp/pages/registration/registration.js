import { Component } from '@angular/core';
import { NavController, MenuController, Events, AlertController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { HomePage } from '../home/home';
import { Config } from '../../providers/config';
import { Userdata } from '../../providers/userdata';
/*
  Generated class for the Registration page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var RegistrationPage = (function () {
    function RegistrationPage(navCtrl, formBuilder, menu, events, http, alertCtrl, config, userData) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.menu = menu;
        this.events = events;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.config = config;
        this.userData = userData;
        this.API_URL = config.API_URL;
        this.regForm = this.formBuilder.group({
            name: ['', Validators.compose([Validators.minLength(3), Validators.required])],
            email: ['', Validators.compose([Validators.minLength(3), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
            password_match: ['', Validators.compose([Validators.minLength(8), Validators.required])]
        });
        this.name = this.regForm.controls['name'];
        this.email = this.regForm.controls['email'];
        this.password = this.regForm.controls['password'];
        this.password_match = this.regForm.controls['password_match'];
    }
    RegistrationPage.prototype.ionViewDidLoad = function () {
        console.log('Hello RegistrationPage Page');
    };
    RegistrationPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(false);
    };
    RegistrationPage.prototype.ionViewCanLeave = function () {
        this.menu.enable(true);
    };
    RegistrationPage.prototype.signUp = function () {
        var _this = this;
        console.log(this.regForm.value);
        if (this.regForm.valid) {
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            var value = this.regForm.value;
            this.http.post(this.API_URL + '/api/register', JSON.stringify(value), { headers: headers })
                .subscribe(function (data) {
                console.log(data.json());
                var res = data.json();
                var user = res.data;
                _this.userData.signup(user);
                _this.navCtrl.setRoot(HomePage);
            }, function (err) {
                var err = err.json();
                var alert = _this.alertCtrl.create({
                    title: 'Register Failed',
                    subTitle: err.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    RegistrationPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-registration',
                    templateUrl: 'registration.html'
                },] },
    ];
    /** @nocollapse */
    RegistrationPage.ctorParameters = [
        { type: NavController, },
        { type: FormBuilder, },
        { type: MenuController, },
        { type: Events, },
        { type: Http, },
        { type: AlertController, },
        { type: Config, },
        { type: Userdata, },
    ];
    return RegistrationPage;
}());
//# sourceMappingURL=registration.js.map