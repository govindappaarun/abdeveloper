import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the EditAds page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-edit-ads',
  templateUrl: 'edit-ads.html'
})
export class EditAdsPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello EditAdsPage Page');
  }

}
