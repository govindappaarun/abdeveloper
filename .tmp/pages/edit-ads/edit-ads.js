import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/*
  Generated class for the EditAds page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var EditAdsPage = (function () {
    function EditAdsPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    EditAdsPage.prototype.ionViewDidLoad = function () {
        console.log('Hello EditAdsPage Page');
    };
    EditAdsPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-edit-ads',
                    templateUrl: 'edit-ads.html'
                },] },
    ];
    /** @nocollapse */
    EditAdsPage.ctorParameters = [
        { type: NavController, },
    ];
    return EditAdsPage;
}());
//# sourceMappingURL=edit-ads.js.map