/**
 * This file is generated by the Angular 2 template compiler.
 * Do not edit.
 */
/* tslint:disable */
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
import * as import0 from './home';
import * as import1 from '@angular/core/src/linker/view';
import * as import3 from '@angular/core/src/linker/element';
import * as import4 from '@angular/core/src/linker/view_utils';
import * as import6 from '@angular/core/src/linker/view_type';
import * as import7 from '@angular/core/src/change_detection/change_detection';
import * as import8 from 'ionic-angular/navigation/nav-controller';
import * as import9 from 'ionic-angular/components/modal/modal';
import * as import10 from 'ionic-angular/components/popover/popover';
import * as import11 from '@angular/core/src/metadata/view';
import * as import12 from '@angular/core/src/linker/component_factory';
import * as import13 from '../../node_modules/ionic-angular/components/toolbar/toolbar.ngfactory';
import * as import14 from '../../node_modules/ionic-angular/components/navbar/navbar.ngfactory';
import * as import15 from '../../node_modules/ionic-angular/components/button/button.ngfactory';
import * as import16 from '../../node_modules/ionic-angular/components/menu/menu-toggle.ngfactory';
import * as import17 from '../../node_modules/ionic-angular/components/toolbar/toolbar-item.ngfactory';
import * as import18 from '@angular/core/src/linker/query_list';
import * as import19 from '../../node_modules/ionic-angular/components/icon/icon.ngfactory';
import * as import20 from '../../node_modules/ionic-angular/components/content/content.ngfactory';
import * as import21 from '../../providers/config';
import * as import22 from '../../components/map/map.ngfactory';
import * as import23 from 'ionic-angular/config/config';
import * as import24 from '@angular/core/src/linker/element_ref';
import * as import25 from 'ionic-angular/navigation/view-controller';
import * as import26 from 'ionic-angular/components/app/app';
import * as import27 from 'ionic-angular/components/menu/menu-controller';
import * as import28 from 'ionic-angular/components/toolbar/toolbar';
import * as import29 from 'ionic-angular/util/keyboard';
import * as import30 from '@angular/core/src/zone/ng_zone';
import * as import31 from 'ionic-angular/components/tabs/tabs';
import * as import32 from '@angular/http/src/http';
import * as import33 from 'ionic-angular/components/alert/alert';
import * as import34 from 'ionic-angular/components/loading/loading';
import * as import35 from 'ionic-angular/components/icon/icon';
import * as import36 from 'ionic-angular/components/button/button';
import * as import37 from 'ionic-angular/components/menu/menu-toggle';
import * as import38 from 'ionic-angular/components/toolbar/toolbar-item';
import * as import39 from 'ionic-angular/components/navbar/navbar';
import * as import40 from '../../components/map/map';
import * as import41 from 'ionic-angular/components/content/content';
export var Wrapper_HomePage = (function () {
    function Wrapper_HomePage(p0, p1, p2) {
        this.changed = false;
        this.context = new import0.HomePage(p0, p1, p2);
    }
    Wrapper_HomePage.prototype.detectChangesInternal = function (view, el, throwOnChange) {
        var changed = this.changed;
        this.changed = false;
        return changed;
    };
    return Wrapper_HomePage;
}());
var renderType_HomePage_Host = null;
var _View_HomePage_Host0 = (function (_super) {
    __extends(_View_HomePage_Host0, _super);
    function _View_HomePage_Host0(viewUtils, parentInjector, declarationEl) {
        _super.call(this, _View_HomePage_Host0, renderType_HomePage_Host, import6.ViewType.HOST, viewUtils, parentInjector, declarationEl, import7.ChangeDetectorStatus.CheckAlways);
    }
    _View_HomePage_Host0.prototype.createInternal = function (rootSelector) {
        this._el_0 = this.selectOrCreateHostElement('page-home', rootSelector, null);
        this._appEl_0 = new import3.AppElement(0, null, this, this._el_0);
        var compView_0 = viewFactory_HomePage0(this.viewUtils, this.injector(0), this._appEl_0);
        this._HomePage_0_4 = new Wrapper_HomePage(this.parentInjector.get(import8.NavController), this.parentInjector.get(import9.ModalController), this.parentInjector.get(import10.PopoverController));
        this._appEl_0.initComponent(this._HomePage_0_4.context, [], compView_0);
        compView_0.create(this._HomePage_0_4.context, this.projectableNodes, null);
        this.init([].concat([this._el_0]), [this._el_0], [], []);
        return this._appEl_0;
    };
    _View_HomePage_Host0.prototype.injectorGetInternal = function (token, requestNodeIndex, notFoundResult) {
        if (((token === import0.HomePage) && (0 === requestNodeIndex))) {
            return this._HomePage_0_4.context;
        }
        return notFoundResult;
    };
    _View_HomePage_Host0.prototype.detectChangesInternal = function (throwOnChange) {
        this._HomePage_0_4.detectChangesInternal(this, this._el_0, throwOnChange);
        this.detectContentChildrenChanges(throwOnChange);
        this.detectViewChildrenChanges(throwOnChange);
    };
    return _View_HomePage_Host0;
}(import1.AppView));
function viewFactory_HomePage_Host0(viewUtils, parentInjector, declarationEl) {
    if ((renderType_HomePage_Host === null)) {
        (renderType_HomePage_Host = viewUtils.createRenderComponentType('', 0, import11.ViewEncapsulation.None, [], {}));
    }
    return new _View_HomePage_Host0(viewUtils, parentInjector, declarationEl);
}
export var HomePageNgFactory = new import12.ComponentFactory('page-home', viewFactory_HomePage_Host0, import0.HomePage);
var styles_HomePage = [];
var renderType_HomePage = null;
var _View_HomePage0 = (function (_super) {
    __extends(_View_HomePage0, _super);
    function _View_HomePage0(viewUtils, parentInjector, declarationEl) {
        _super.call(this, _View_HomePage0, renderType_HomePage, import6.ViewType.COMPONENT, viewUtils, parentInjector, declarationEl, import7.ChangeDetectorStatus.CheckAlways);
    }
    _View_HomePage0.prototype.createInternal = function (rootSelector) {
        var parentRenderNode = this.renderer.createViewRoot(this.declarationAppElement.nativeElement);
        this._text_0 = this.renderer.createText(parentRenderNode, '\n\n', null);
        this._el_1 = this.renderer.createElement(parentRenderNode, 'ion-header', null);
        this._Header_1_3 = new import13.Wrapper_Header(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_1), this.renderer, this.parentInjector.get(import25.ViewController, null));
        this._text_2 = this.renderer.createText(this._el_1, '\n  ', null);
        this._el_3 = this.renderer.createElement(this._el_1, 'ion-navbar', null);
        this.renderer.setElementAttribute(this._el_3, 'class', 'toolbar');
        this.renderer.setElementAttribute(this._el_3, 'color', 'primary');
        this.renderer.setElementAttribute(this._el_3, 'no-border-bottom', '');
        this._appEl_3 = new import3.AppElement(3, 1, this, this._el_3);
        var compView_3 = import14.viewFactory_Navbar0(this.viewUtils, this.injector(3), this._appEl_3);
        this._Navbar_3_4 = new import14.Wrapper_Navbar(this.parentInjector.get(import26.App), this.parentInjector.get(import25.ViewController, null), this.parentInjector.get(import8.NavController, null), this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_3), this.renderer);
        this._appEl_3.initComponent(this._Navbar_3_4.context, [], compView_3);
        this._text_4 = this.renderer.createText(null, '\n    ', null);
        this._el_5 = this.renderer.createElement(null, 'button', null);
        this.renderer.setElementAttribute(this._el_5, 'ion-button', '');
        this.renderer.setElementAttribute(this._el_5, 'menuToggle', '');
        this._appEl_5 = new import3.AppElement(5, 3, this, this._el_5);
        var compView_5 = import15.viewFactory_Button0(this.viewUtils, this.injector(5), this._appEl_5);
        this._Button_5_4 = new import15.Wrapper_Button('', '', this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_5), this.renderer);
        this._MenuToggle_5_5 = new import16.Wrapper_MenuToggle(this.parentInjector.get(import27.MenuController), new import24.ElementRef(this._el_5), this.parentInjector.get(import25.ViewController, null), this._Navbar_3_4.context);
        this._ToolbarItem_5_6 = new import17.Wrapper_ToolbarItem(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_5), this.renderer, this.parentInjector.get(import28.Toolbar, null), this._Navbar_3_4.context);
        this._query_Button_5_0 = new import18.QueryList();
        this._appEl_5.initComponent(this._Button_5_4.context, [], compView_5);
        this._text_6 = this.renderer.createText(null, '\n      ', null);
        this._el_7 = this.renderer.createElement(null, 'ion-icon', null);
        this.renderer.setElementAttribute(this._el_7, 'name', 'menu');
        this.renderer.setElementAttribute(this._el_7, 'role', 'img');
        this._Icon_7_3 = new import19.Wrapper_Icon(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_7), this.renderer);
        this._text_8 = this.renderer.createText(null, '\n    ', null);
        compView_5.create(this._Button_5_4.context, [[].concat([
                this._text_6,
                this._el_7,
                this._text_8
            ])], null);
        this._text_9 = this.renderer.createText(null, '\n\n    \n        ', null);
        this._el_10 = this.renderer.createElement(null, 'p', null);
        this.renderer.setElementAttribute(this._el_10, 'style', 'color:yellow;');
        this._text_11 = this.renderer.createText(this._el_10, 'Home', null);
        this._text_12 = this.renderer.createText(null, '\n     \n    ', null);
        this._el_13 = this.renderer.createElement(null, 'ion-buttons', null);
        this.renderer.setElementAttribute(this._el_13, 'end', '');
        this._ToolbarItem_13_3 = new import17.Wrapper_ToolbarItem(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_13), this.renderer, this.parentInjector.get(import28.Toolbar, null), this._Navbar_3_4.context);
        this._query_Button_13_0 = new import18.QueryList();
        this._text_14 = this.renderer.createText(this._el_13, '\n      ', null);
        this._el_15 = this.renderer.createElement(this._el_13, 'button', null);
        this.renderer.setElementAttribute(this._el_15, 'icon-only', '');
        this.renderer.setElementAttribute(this._el_15, 'ion-button', '');
        this._appEl_15 = new import3.AppElement(15, 13, this, this._el_15);
        var compView_15 = import15.viewFactory_Button0(this.viewUtils, this.injector(15), this._appEl_15);
        this._Button_15_4 = new import15.Wrapper_Button(null, '', this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_15), this.renderer);
        this._appEl_15.initComponent(this._Button_15_4.context, [], compView_15);
        this._text_16 = this.renderer.createText(null, '\n        ', null);
        this._el_17 = this.renderer.createElement(null, 'ion-icon', null);
        this.renderer.setElementAttribute(this._el_17, 'ios', 'ios-options-outline');
        this.renderer.setElementAttribute(this._el_17, 'md', 'md-options');
        this.renderer.setElementAttribute(this._el_17, 'role', 'img');
        this._Icon_17_3 = new import19.Wrapper_Icon(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_17), this.renderer);
        this._text_18 = this.renderer.createText(null, '\n      ', null);
        compView_15.create(this._Button_15_4.context, [[].concat([
                this._text_16,
                this._el_17,
                this._text_18
            ])], null);
        this._text_19 = this.renderer.createText(this._el_13, '\n    ', null);
        this._text_20 = this.renderer.createText(null, '\n  ', null);
        compView_3.create(this._Navbar_3_4.context, [
            [].concat([this._el_5]),
            [],
            [].concat([this._el_13]),
            [].concat([
                this._text_4,
                this._text_9,
                this._el_10,
                this._text_12,
                this._text_20
            ])
        ], null);
        this._text_21 = this.renderer.createText(this._el_1, '\n\n  ', null);
        this._text_22 = this.renderer.createText(this._el_1, '\n\n   \n', null);
        this._text_23 = this.renderer.createText(parentRenderNode, '\n\n', null);
        this._el_24 = this.renderer.createElement(parentRenderNode, 'ion-content', null);
        this._appEl_24 = new import3.AppElement(24, null, this, this._el_24);
        var compView_24 = import20.viewFactory_Content0(this.viewUtils, this.injector(24), this._appEl_24);
        this._Content_24_4 = new import20.Wrapper_Content(this.parentInjector.get(import23.Config), new import24.ElementRef(this._el_24), this.renderer, this.parentInjector.get(import26.App), this.parentInjector.get(import29.Keyboard), this.parentInjector.get(import30.NgZone), this.parentInjector.get(import25.ViewController, null), this.parentInjector.get(import31.Tabs, null));
        this._appEl_24.initComponent(this._Content_24_4.context, [], compView_24);
        this._text_25 = this.renderer.createText(null, '\n  ', null);
        this._el_26 = this.renderer.createElement(null, 'div', null);
        this.renderer.setElementAttribute(this._el_26, 'class', 'map-wrapper');
        this._text_27 = this.renderer.createText(this._el_26, '\n     ', null);
        this._el_28 = this.renderer.createElement(this._el_26, 'map', null);
        this._appEl_28 = new import3.AppElement(28, 26, this, this._el_28);
        var compView_28 = import22.viewFactory_MapComponent0(this.viewUtils, this.injector(28), this._appEl_28);
        this._Config_28_4 = new import21.Config();
        this._MapComponent_28_5 = new import22.Wrapper_MapComponent(this.parentInjector.get(import8.NavController), this.parentInjector.get(import32.Http), this.parentInjector.get(import33.AlertController), this.parentInjector.get(import34.LoadingController), this.parentInjector.get(import9.ModalController), this._Config_28_4);
        this._appEl_28.initComponent(this._MapComponent_28_5.context, [], compView_28);
        compView_28.create(this._MapComponent_28_5.context, [], null);
        this._text_29 = this.renderer.createText(this._el_26, '\n  ', null);
        this._text_30 = this.renderer.createText(null, '\n', null);
        compView_24.create(this._Content_24_4.context, [
            [],
            [].concat([
                this._text_25,
                this._el_26,
                this._text_30
            ]),
            []
        ], null);
        this._text_31 = this.renderer.createText(parentRenderNode, '\n', null);
        this._expr_1 = import7.UNINITIALIZED;
        this._expr_2 = import7.UNINITIALIZED;
        var disposable_0 = this.renderer.listen(this._el_5, 'click', this.eventHandler(this._handle_click_5_0.bind(this)));
        this._expr_5 = import7.UNINITIALIZED;
        this._expr_7 = import7.UNINITIALIZED;
        var disposable_1 = this.renderer.listen(this._el_15, 'click', this.eventHandler(this._handle_click_15_0.bind(this)));
        this._expr_11 = import7.UNINITIALIZED;
        this._expr_12 = import7.UNINITIALIZED;
        this.init([], [
            this._text_0,
            this._el_1,
            this._text_2,
            this._el_3,
            this._text_4,
            this._el_5,
            this._text_6,
            this._el_7,
            this._text_8,
            this._text_9,
            this._el_10,
            this._text_11,
            this._text_12,
            this._el_13,
            this._text_14,
            this._el_15,
            this._text_16,
            this._el_17,
            this._text_18,
            this._text_19,
            this._text_20,
            this._text_21,
            this._text_22,
            this._text_23,
            this._el_24,
            this._text_25,
            this._el_26,
            this._text_27,
            this._el_28,
            this._text_29,
            this._text_30,
            this._text_31
        ], [
            disposable_0,
            disposable_1
        ], []);
        return null;
    };
    _View_HomePage0.prototype.injectorGetInternal = function (token, requestNodeIndex, notFoundResult) {
        if (((token === import35.Icon) && (7 === requestNodeIndex))) {
            return this._Icon_7_3.context;
        }
        if (((token === import36.Button) && ((5 <= requestNodeIndex) && (requestNodeIndex <= 8)))) {
            return this._Button_5_4.context;
        }
        if (((token === import37.MenuToggle) && ((5 <= requestNodeIndex) && (requestNodeIndex <= 8)))) {
            return this._MenuToggle_5_5.context;
        }
        if (((token === import38.ToolbarItem) && ((5 <= requestNodeIndex) && (requestNodeIndex <= 8)))) {
            return this._ToolbarItem_5_6.context;
        }
        if (((token === import35.Icon) && (17 === requestNodeIndex))) {
            return this._Icon_17_3.context;
        }
        if (((token === import36.Button) && ((15 <= requestNodeIndex) && (requestNodeIndex <= 18)))) {
            return this._Button_15_4.context;
        }
        if (((token === import38.ToolbarItem) && ((13 <= requestNodeIndex) && (requestNodeIndex <= 19)))) {
            return this._ToolbarItem_13_3.context;
        }
        if (((token === import39.Navbar) && ((3 <= requestNodeIndex) && (requestNodeIndex <= 20)))) {
            return this._Navbar_3_4.context;
        }
        if (((token === import28.Header) && ((1 <= requestNodeIndex) && (requestNodeIndex <= 22)))) {
            return this._Header_1_3.context;
        }
        if (((token === import21.Config) && (28 === requestNodeIndex))) {
            return this._Config_28_4;
        }
        if (((token === import40.MapComponent) && (28 === requestNodeIndex))) {
            return this._MapComponent_28_5.context;
        }
        if (((token === import41.Content) && ((24 <= requestNodeIndex) && (requestNodeIndex <= 30)))) {
            return this._Content_24_4.context;
        }
        return notFoundResult;
    };
    _View_HomePage0.prototype.detectChangesInternal = function (throwOnChange) {
        this._Header_1_3.detectChangesInternal(this, this._el_1, throwOnChange);
        var currVal_0 = 'primary';
        this._Navbar_3_4.check_color(currVal_0, throwOnChange, false);
        this._Navbar_3_4.detectChangesInternal(this, this._el_3, throwOnChange);
        if (this._Button_5_4.detectChangesInternal(this, this._el_5, throwOnChange)) {
            this._appEl_5.componentView.markAsCheckOnce();
        }
        var currVal_4 = '';
        this._MenuToggle_5_5.check_menuToggle(currVal_4, throwOnChange, false);
        this._MenuToggle_5_5.detectChangesInternal(this, this._el_5, throwOnChange);
        this._ToolbarItem_5_6.detectChangesInternal(this, this._el_5, throwOnChange);
        var currVal_6 = 'menu';
        this._Icon_7_3.check_name(currVal_6, throwOnChange, false);
        this._Icon_7_3.detectChangesInternal(this, this._el_7, throwOnChange);
        this._ToolbarItem_13_3.detectChangesInternal(this, this._el_13, throwOnChange);
        if (this._Button_15_4.detectChangesInternal(this, this._el_15, throwOnChange)) {
            this._appEl_15.componentView.markAsCheckOnce();
        }
        var currVal_9 = 'ios-options-outline';
        this._Icon_17_3.check_ios(currVal_9, throwOnChange, false);
        var currVal_10 = 'md-options';
        this._Icon_17_3.check_md(currVal_10, throwOnChange, false);
        this._Icon_17_3.detectChangesInternal(this, this._el_17, throwOnChange);
        if (this._Content_24_4.detectChangesInternal(this, this._el_24, throwOnChange)) {
            this._appEl_24.componentView.markAsCheckOnce();
        }
        this._MapComponent_28_5.detectChangesInternal(this, this._el_28, throwOnChange);
        this.detectContentChildrenChanges(throwOnChange);
        if (!throwOnChange) {
            if (this._query_Button_5_0.dirty) {
                this._query_Button_5_0.reset([this._Button_5_4.context]);
                this._ToolbarItem_5_6.context._buttons = this._query_Button_5_0;
                this._query_Button_5_0.notifyOnChanges();
            }
            if (this._query_Button_13_0.dirty) {
                this._query_Button_13_0.reset([this._Button_15_4.context]);
                this._ToolbarItem_13_3.context._buttons = this._query_Button_13_0;
                this._query_Button_13_0.notifyOnChanges();
            }
            if ((this.numberOfChecks === 0)) {
                this._Button_5_4.context.ngAfterContentInit();
            }
            if ((this.numberOfChecks === 0)) {
                this._Button_15_4.context.ngAfterContentInit();
            }
        }
        var currVal_1 = this._Navbar_3_4.context._hidden;
        if (import4.checkBinding(throwOnChange, this._expr_1, currVal_1)) {
            this.renderer.setElementProperty(this._el_3, 'hidden', currVal_1);
            this._expr_1 = currVal_1;
        }
        var currVal_2 = this._Navbar_3_4.context._sbPadding;
        if (import4.checkBinding(throwOnChange, this._expr_2, currVal_2)) {
            this.renderer.setElementClass(this._el_3, 'statusbar-padding', currVal_2);
            this._expr_2 = currVal_2;
        }
        var currVal_5 = this._MenuToggle_5_5.context.isHidden;
        if (import4.checkBinding(throwOnChange, this._expr_5, currVal_5)) {
            this.renderer.setElementProperty(this._el_5, 'hidden', currVal_5);
            this._expr_5 = currVal_5;
        }
        var currVal_7 = this._Icon_7_3.context._hidden;
        if (import4.checkBinding(throwOnChange, this._expr_7, currVal_7)) {
            this.renderer.setElementClass(this._el_7, 'hide', currVal_7);
            this._expr_7 = currVal_7;
        }
        var currVal_11 = this._Icon_17_3.context._hidden;
        if (import4.checkBinding(throwOnChange, this._expr_11, currVal_11)) {
            this.renderer.setElementClass(this._el_17, 'hide', currVal_11);
            this._expr_11 = currVal_11;
        }
        var currVal_12 = this._Content_24_4.context._sbPadding;
        if (import4.checkBinding(throwOnChange, this._expr_12, currVal_12)) {
            this.renderer.setElementClass(this._el_24, 'statusbar-padding', currVal_12);
            this._expr_12 = currVal_12;
        }
        this.detectViewChildrenChanges(throwOnChange);
        if (!throwOnChange) {
            if ((this.numberOfChecks === 0)) {
                this._Navbar_3_4.context.ngAfterViewInit();
            }
        }
    };
    _View_HomePage0.prototype.destroyInternal = function () {
        this._Icon_7_3.context.ngOnDestroy();
        this._Icon_17_3.context.ngOnDestroy();
        this._Content_24_4.context.ngOnDestroy();
    };
    _View_HomePage0.prototype._handle_click_5_0 = function ($event) {
        this.markPathToRootAsCheckOnce();
        var pd_0 = (this._MenuToggle_5_5.context.toggle() !== false);
        return (true && pd_0);
    };
    _View_HomePage0.prototype._handle_click_15_0 = function ($event) {
        this.markPathToRootAsCheckOnce();
        var pd_0 = (this.context.presentPopover($event) !== false);
        return (true && pd_0);
    };
    return _View_HomePage0;
}(import1.AppView));
export function viewFactory_HomePage0(viewUtils, parentInjector, declarationEl) {
    if ((renderType_HomePage === null)) {
        (renderType_HomePage = viewUtils.createRenderComponentType('', 0, import11.ViewEncapsulation.None, styles_HomePage, {}));
    }
    return new _View_HomePage0(viewUtils, parentInjector, declarationEl);
}
//# sourceMappingURL=home.ngfactory.js.map