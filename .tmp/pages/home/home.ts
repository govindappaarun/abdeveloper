import { Component} from '@angular/core';

import { NavController, ModalController, PopoverController } from 'ionic-angular';
import {PopcoverpagePage} from '../popcoverpage/popcoverpage';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
   segment = 'all';
  
  constructor(public navCtrl: NavController, public modalCtrl: ModalController,
             private popoverCtrl: PopoverController) {}

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopcoverpagePage);
    popover.present({
      ev: myEvent
    });
  }

}
