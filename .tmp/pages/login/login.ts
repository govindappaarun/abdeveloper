import { Component} from '@angular/core';
import { NavController, MenuController, Events, AlertController } from 'ionic-angular';
import {Validators, FormBuilder,FormGroup, AbstractControl } from '@angular/forms';
import { Http, Headers } from '@angular/http';

import { HomePage } from '../home/home';
import {Config} from '../../providers/config';
import {Userdata} from '../../providers/userdata';

/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers:[Config]
})
export class LoginPage {

  logForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;

  public API_URL: string;

  constructor(public navCtrl: NavController, 
                 private formBuilder: FormBuilder, 
                   public menu: MenuController, 
                     public events: Events, public http: Http,
                       public alertCtrl: AlertController, public config: Config, public userData: Userdata ) {
                      console.log(config.API_URL)
    this.API_URL = config.API_URL;
    this.logForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.minLength(3),Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])]      
    });
    this.email = this.logForm.controls['email'];
    this.password = this.logForm.controls['password'];
  }

  ionViewDidLoad() {
    console.log('Hello LoginPage Page');
  }

  ionViewDidEnter(){
    this.menu.enable(false);
  }
  ionViewCanLeave(){
    this.menu.enable(true);
  }

  login(){
    //console.log(this.logForm.value);
    if(this.logForm.valid) {
          var value = this.logForm.value;
          let headers = new Headers();
          headers.append('Content-Type', 'application/json');
          this.http.post(this.API_URL + '/api/login', JSON.stringify(value), {headers: headers})
            .subscribe(
               data => {
                  //console.log(data.json());
                  var res = data.json() ;
                  var user = res.data ;
                  this.userData.login(user);
                  this.navCtrl.setRoot(HomePage);
                },
              err => {
                  var err = err.json();
                  let alert = this.alertCtrl.create({
                     title: 'Login Failed',
                     subTitle: err.message,
                     buttons: ['OK']
                  });
                  alert.present();
              }
          );
                     
    }
  }
}
