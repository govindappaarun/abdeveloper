import { Component } from '@angular/core';
import { NavController, MenuController, Events, AlertController } from 'ionic-angular';
import { Validators, FormBuilder } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { HomePage } from '../home/home';
import { Config } from '../../providers/config';
import { Userdata } from '../../providers/userdata';
/*
  Generated class for the Login page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var LoginPage = (function () {
    function LoginPage(navCtrl, formBuilder, menu, events, http, alertCtrl, config, userData) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.menu = menu;
        this.events = events;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.config = config;
        this.userData = userData;
        console.log(config.API_URL);
        this.API_URL = config.API_URL;
        this.logForm = this.formBuilder.group({
            email: ['', Validators.compose([Validators.minLength(3), Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'), Validators.required])],
            password: ['', Validators.compose([Validators.minLength(8), Validators.required])]
        });
        this.email = this.logForm.controls['email'];
        this.password = this.logForm.controls['password'];
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('Hello LoginPage Page');
    };
    LoginPage.prototype.ionViewDidEnter = function () {
        this.menu.enable(false);
    };
    LoginPage.prototype.ionViewCanLeave = function () {
        this.menu.enable(true);
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        //console.log(this.logForm.value);
        if (this.logForm.valid) {
            var value = this.logForm.value;
            var headers = new Headers();
            headers.append('Content-Type', 'application/json');
            this.http.post(this.API_URL + '/api/login', JSON.stringify(value), { headers: headers })
                .subscribe(function (data) {
                //console.log(data.json());
                var res = data.json();
                var user = res.data;
                _this.userData.login(user);
                _this.navCtrl.setRoot(HomePage);
            }, function (err) {
                var err = err.json();
                var alert = _this.alertCtrl.create({
                    title: 'Login Failed',
                    subTitle: err.message,
                    buttons: ['OK']
                });
                alert.present();
            });
        }
    };
    LoginPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-login',
                    templateUrl: 'login.html',
                    providers: [Config]
                },] },
    ];
    /** @nocollapse */
    LoginPage.ctorParameters = [
        { type: NavController, },
        { type: FormBuilder, },
        { type: MenuController, },
        { type: Events, },
        { type: Http, },
        { type: AlertController, },
        { type: Config, },
        { type: Userdata, },
    ];
    return LoginPage;
}());
//# sourceMappingURL=login.js.map