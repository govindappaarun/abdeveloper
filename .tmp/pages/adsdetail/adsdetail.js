import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
/*
  Generated class for the Adsdetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var AdsdetailPage = (function () {
    function AdsdetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.ad = navParams.get('ad');
    }
    AdsdetailPage.prototype.ionViewDidLoad = function () {
        console.log('Hello AdsdetailPage Page');
    };
    AdsdetailPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-adsdetail',
                    templateUrl: 'adsdetail.html'
                },] },
    ];
    /** @nocollapse */
    AdsdetailPage.ctorParameters = [
        { type: NavController, },
        { type: NavParams, },
    ];
    return AdsdetailPage;
}());
//# sourceMappingURL=adsdetail.js.map