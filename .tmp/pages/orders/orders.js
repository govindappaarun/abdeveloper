import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/*
  Generated class for the Orders page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var OrdersPage = (function () {
    function OrdersPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    OrdersPage.prototype.ionViewDidLoad = function () {
        console.log('Hello OrdersPage Page');
    };
    OrdersPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-orders',
                    templateUrl: 'orders.html'
                },] },
    ];
    /** @nocollapse */
    OrdersPage.ctorParameters = [
        { type: NavController, },
    ];
    return OrdersPage;
}());
//# sourceMappingURL=orders.js.map