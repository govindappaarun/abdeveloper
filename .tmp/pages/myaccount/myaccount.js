import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/*
  Generated class for the Myaccount page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var MyaccountPage = (function () {
    function MyaccountPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    MyaccountPage.prototype.ionViewDidLoad = function () {
        console.log('Hello MyaccountPage Page');
    };
    MyaccountPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-myaccount',
                    templateUrl: 'myaccount.html'
                },] },
    ];
    /** @nocollapse */
    MyaccountPage.ctorParameters = [
        { type: NavController, },
    ];
    return MyaccountPage;
}());
//# sourceMappingURL=myaccount.js.map