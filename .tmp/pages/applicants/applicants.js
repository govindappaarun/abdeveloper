import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/*
  Generated class for the Applicants page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var ApplicantsPage = (function () {
    function ApplicantsPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.type = "sellers";
    }
    ApplicantsPage.prototype.ionViewDidLoad = function () {
        console.log('Hello ApplicantsPage Page');
    };
    ApplicantsPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-applicants',
                    templateUrl: 'applicants.html'
                },] },
    ];
    /** @nocollapse */
    ApplicantsPage.ctorParameters = [
        { type: NavController, },
    ];
    return ApplicantsPage;
}());
//# sourceMappingURL=applicants.js.map