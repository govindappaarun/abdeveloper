import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';


@Component({
  selector: 'page-popcoverpage',
  templateUrl: 'popcoverpage.html'
})

export class PopcoverpagePage {
  filters: Array<{name: string, isChecked: boolean}> = [];
  constructor(public viewCtrl: ViewController) {
    this.filters = [
      { name : 'BillBoard' , isChecked: true},
      { name : 'News Paper' , isChecked: true},
      { name : 'Radio' , isChecked: true},
      { name : 'TV' , isChecked: true},
      { name : 'Website' , isChecked: true},
      { name : 'Mobile' , isChecked: true},
    ]
  }

  close() {
    this.viewCtrl.dismiss();
  }
}
