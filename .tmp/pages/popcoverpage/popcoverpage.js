import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
export var PopcoverpagePage = (function () {
    function PopcoverpagePage(viewCtrl) {
        this.viewCtrl = viewCtrl;
        this.filters = [];
        this.filters = [
            { name: 'BillBoard', isChecked: true },
            { name: 'News Paper', isChecked: true },
            { name: 'Radio', isChecked: true },
            { name: 'TV', isChecked: true },
            { name: 'Website', isChecked: true },
            { name: 'Mobile', isChecked: true },
        ];
    }
    PopcoverpagePage.prototype.close = function () {
        this.viewCtrl.dismiss();
    };
    PopcoverpagePage.decorators = [
        { type: Component, args: [{
                    selector: 'page-popcoverpage',
                    templateUrl: 'popcoverpage.html'
                },] },
    ];
    /** @nocollapse */
    PopcoverpagePage.ctorParameters = [
        { type: ViewController, },
    ];
    return PopcoverpagePage;
}());
//# sourceMappingURL=popcoverpage.js.map