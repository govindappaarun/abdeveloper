import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Cartpage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-cartpage',
  templateUrl: 'cartpage.html'
})
export class Cartpage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello CartpagePage Page');
  }

}
