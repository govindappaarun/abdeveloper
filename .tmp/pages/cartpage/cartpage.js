import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
/*
  Generated class for the Cartpage page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var Cartpage = (function () {
    function Cartpage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    Cartpage.prototype.ionViewDidLoad = function () {
        console.log('Hello CartpagePage Page');
    };
    Cartpage.decorators = [
        { type: Component, args: [{
                    selector: 'page-cartpage',
                    templateUrl: 'cartpage.html'
                },] },
    ];
    /** @nocollapse */
    Cartpage.ctorParameters = [
        { type: NavController, },
    ];
    return Cartpage;
}());
//# sourceMappingURL=cartpage.js.map