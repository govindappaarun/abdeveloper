import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { AdvertisementService } from '../../providers/advertisement';
/*
  Generated class for the ViewAds page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-view-ads',
  templateUrl: 'view-ads.html'
})
export class ViewAdsPage {
  public username: string;
  ads:any;

  constructor(public navCtrl: NavController, public advertisementService: AdvertisementService) {
    var user = JSON.parse(window.localStorage.getItem('user'));
    this.username = user.email.toString();
    var options ={ owner_user_ID : this.username } ;
    console.log(options);
    
    this.advertisementService.getAdsByOption(options).subscribe(data => {
      this.ads = data;
    });
    //this.ads = this.advertisement.getAds();
    console.log(this.ads);
  }

  ionViewDidLoad() {
    console.log('Hello ViewAdsPage Page');
    
  }

}
