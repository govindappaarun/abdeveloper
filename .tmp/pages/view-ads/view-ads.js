import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AdvertisementService } from '../../providers/advertisement';
/*
  Generated class for the ViewAds page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var ViewAdsPage = (function () {
    function ViewAdsPage(navCtrl, advertisementService) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.advertisementService = advertisementService;
        var user = JSON.parse(window.localStorage.getItem('user'));
        this.username = user.email.toString();
        var options = { owner_user_ID: this.username };
        console.log(options);
        this.advertisementService.getAdsByOption(options).subscribe(function (data) {
            _this.ads = data;
        });
        //this.ads = this.advertisement.getAds();
        console.log(this.ads);
    }
    ViewAdsPage.prototype.ionViewDidLoad = function () {
        console.log('Hello ViewAdsPage Page');
    };
    ViewAdsPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-view-ads',
                    templateUrl: 'view-ads.html'
                },] },
    ];
    /** @nocollapse */
    ViewAdsPage.ctorParameters = [
        { type: NavController, },
        { type: AdvertisementService, },
    ];
    return ViewAdsPage;
}());
//# sourceMappingURL=view-ads.js.map