import { Component, ViewChild} from '@angular/core';
import { NavController, ActionSheetController, Platform, NavParams, AlertController} from 'ionic-angular';
import {FormBuilder,Validators} from '@angular/forms';
import {Camera} from 'ionic-native';
import {Http,Headers} from '@angular/http';
import {Config} from '../../providers/config';
import {HomePage} from '../home/home';

/*
  Generated class for the AddAds page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-add-ads',
  templateUrl: 'add-ads.html',
  providers:[Config]
})

export class AddAdsPage {
  @ViewChild('dateTime1') dateTime1;
  @ViewChild('dateTime2') dateTime2;
 public API_URL: string;
 
 public adsForm ;

    public base64Image: string;
    type: number = 1;
    title: string;
    description: string;
    scale: string = "inches";
    illumination: number;
    sDate: string;
    eDate: string;
    
    location:any = {
        lat  : '',
        lng  : ''
    }; 
      
  constructor(public navCtrl: NavController, private formBuilder: FormBuilder,  
                public actionSheetCtrl: ActionSheetController, 
                public platform: Platform, public http: Http, public config: Config,
                private navParams: NavParams,public alertCtrl: AlertController) {
                  this.API_URL = config.API_URL;
                  this.platform.registerBackButtonAction(function(){
                    this.navCtrl.setRoot(HomePage);
                  },500);
                  this.adsForm = this.formBuilder.group({
                    type: ['',Validators.required],
                    title: ['',Validators.required],
                    description: ['',Validators.required],
                    scale: ['',Validators.required],
                    illuminated: ['',Validators.required],
                    start_time: ['',Validators.required],
                    end_time: ['',Validators.required],
                    ad_duration:['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    currency: ['',Validators.required],
                    postDate: ['',Validators.required],
                    length: ['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    width: ['', Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    height: ['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    elevation: ['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    owner_user_ID: ['',Validators.required],
                    units_available: ['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])],
                    lat: [''],
                    lng: [''],
                    photos: [''],
                    rate: ['',Validators.compose([Validators.required,Validators.pattern('[0-9]{1,5}$')])]      
                  });
                  var user = JSON.parse(window.localStorage.getItem('user'));
                  let user_id = user.email.toString();
                  this.adsForm.controls['owner_user_ID'].setValue(user_id);
                  let current_date = new Date().toISOString();
                  this.adsForm.controls['postDate'].setValue(current_date);
                  this.location = navParams.get('location');
                  if(!!this.location){
                    console.log(this.location.lat +' ' + this.location.lng );
                  }
                }

  ngOnInit(){
    setTimeout(_ => {
      this.dateTime1.setValue(new Date().toISOString());
      this.dateTime2.setValue(new Date().toISOString());
    });
  }
    
  ionViewDidLoad() {
    console.log('Hello AddAdsPage Page');
  }

  openMenu() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Upload Photo',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Gallery',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'folder' : null,
          handler: () => {
            console.log('Gallery clicked');
            this.saveMedia(0); 
          }
        },
        {
          text: 'Camera',
          icon: !this.platform.is('ios') ? 'camera' : null,
          handler: () => {
            console.log('camera clicked');
            this.saveMedia(1); 
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
}

saveMedia(type){
  var options = this.optionsForType(type);
  
      Camera.getPicture(options).then((imageData) => {
      // imageData is a base64 encoded string
        console.log('Picture Captured');
        this.base64Image = "data:image/jpeg;base64," + imageData;
        console.log(this.base64Image);
    }, (err) => {
        console.log(err);
    });
}

optionsForType(type) {
    var source;
    switch (type) {
      case 1:
        source = Camera.PictureSourceType.CAMERA;
        break;
      case 0:
        source = Camera.PictureSourceType.PHOTOLIBRARY;
        break;
    }
    return {
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: source,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      saveToPhotoAlbum: true
  };
}

addAds() {
  if(!!this.location){
    this.adsForm.controls['lat'].setValue(this.location.lat);
    this.adsForm.controls['lng'].setValue(this.location.lng);
  }
  if(!!this.base64Image){
    this.adsForm.controls['photos'].setValue(this.base64Image);
  }
    console.log(this.adsForm.value);
    var value = this.adsForm.value;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.post(this.API_URL + '/api/advertisements', JSON.stringify(value), {headers: headers})
            .subscribe(
               data => {
                  console.log(data.json());
                  var res = data.json();
                  let alert = this.alertCtrl.create({
                     title: 'Successfull',
                     subTitle: res.message,
                     buttons: ['OK']
                  });
                  alert.present();
                  this.navCtrl.setRoot(HomePage)
                },
              err => {
                  var err = err.json();
                  let alert = this.alertCtrl.create({
                     title: 'Failed',
                     subTitle: err.message,
                     buttons: ['OK']
                  });
                  alert.present();
              }
          );
          
  }
}