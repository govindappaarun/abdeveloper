import { Component, ViewChild } from '@angular/core';
import { NavController, ActionSheetController, Platform, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { Camera } from 'ionic-native';
import { Http, Headers } from '@angular/http';
import { Config } from '../../providers/config';
import { HomePage } from '../home/home';
/*
  Generated class for the AddAds page.
  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var AddAdsPage = (function () {
    function AddAdsPage(navCtrl, formBuilder, actionSheetCtrl, platform, http, config, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.formBuilder = formBuilder;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.http = http;
        this.config = config;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.type = 1;
        this.scale = "inches";
        this.location = {
            lat: '',
            lng: ''
        };
        this.API_URL = config.API_URL;
        this.platform.registerBackButtonAction(function () {
            this.navCtrl.setRoot(HomePage);
        }, 500);
        this.adsForm = this.formBuilder.group({
            type: ['', Validators.required],
            title: ['', Validators.required],
            description: ['', Validators.required],
            scale: ['', Validators.required],
            illuminated: ['', Validators.required],
            start_time: ['', Validators.required],
            end_time: ['', Validators.required],
            ad_duration: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            currency: ['', Validators.required],
            postDate: ['', Validators.required],
            length: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            width: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            height: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            elevation: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            owner_user_ID: ['', Validators.required],
            units_available: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])],
            lat: [''],
            lng: [''],
            photos: [''],
            rate: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{1,5}$')])]
        });
        var user = JSON.parse(window.localStorage.getItem('user'));
        var user_id = user.email.toString();
        this.adsForm.controls['owner_user_ID'].setValue(user_id);
        var current_date = new Date().toISOString();
        this.adsForm.controls['postDate'].setValue(current_date);
        this.location = navParams.get('location');
        if (!!this.location) {
            console.log(this.location.lat + ' ' + this.location.lng);
        }
    }
    AddAdsPage.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function (_) {
            _this.dateTime1.setValue(new Date().toISOString());
            _this.dateTime2.setValue(new Date().toISOString());
        });
    };
    AddAdsPage.prototype.ionViewDidLoad = function () {
        console.log('Hello AddAdsPage Page');
    };
    AddAdsPage.prototype.openMenu = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: 'Upload Photo',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Gallery',
                    role: 'destructive',
                    icon: !this.platform.is('ios') ? 'folder' : null,
                    handler: function () {
                        console.log('Gallery clicked');
                        _this.saveMedia(0);
                    }
                },
                {
                    text: 'Camera',
                    icon: !this.platform.is('ios') ? 'camera' : null,
                    handler: function () {
                        console.log('camera clicked');
                        _this.saveMedia(1);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel',
                    icon: !this.platform.is('ios') ? 'close' : null,
                    handler: function () {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AddAdsPage.prototype.saveMedia = function (type) {
        var _this = this;
        var options = this.optionsForType(type);
        Camera.getPicture(options).then(function (imageData) {
            // imageData is a base64 encoded string
            console.log('Picture Captured');
            _this.base64Image = "data:image/jpeg;base64," + imageData;
            console.log(_this.base64Image);
        }, function (err) {
            console.log(err);
        });
    };
    AddAdsPage.prototype.optionsForType = function (type) {
        var source;
        switch (type) {
            case 1:
                source = Camera.PictureSourceType.CAMERA;
                break;
            case 0:
                source = Camera.PictureSourceType.PHOTOLIBRARY;
                break;
        }
        return {
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: source,
            allowEdit: true,
            encodingType: Camera.EncodingType.JPEG,
            saveToPhotoAlbum: true
        };
    };
    AddAdsPage.prototype.addAds = function () {
        var _this = this;
        if (!!this.location) {
            this.adsForm.controls['lat'].setValue(this.location.lat);
            this.adsForm.controls['lng'].setValue(this.location.lng);
        }
        if (!!this.base64Image) {
            this.adsForm.controls['photos'].setValue(this.base64Image);
        }
        console.log(this.adsForm.value);
        var value = this.adsForm.value;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post(this.API_URL + '/api/advertisements', JSON.stringify(value), { headers: headers })
            .subscribe(function (data) {
            console.log(data.json());
            var res = data.json();
            var alert = _this.alertCtrl.create({
                title: 'Successfull',
                subTitle: res.message,
                buttons: ['OK']
            });
            alert.present();
            _this.navCtrl.setRoot(HomePage);
        }, function (err) {
            var err = err.json();
            var alert = _this.alertCtrl.create({
                title: 'Failed',
                subTitle: err.message,
                buttons: ['OK']
            });
            alert.present();
        });
    };
    AddAdsPage.decorators = [
        { type: Component, args: [{
                    selector: 'page-add-ads',
                    templateUrl: 'add-ads.html',
                    providers: [Config]
                },] },
    ];
    /** @nocollapse */
    AddAdsPage.ctorParameters = [
        { type: NavController, },
        { type: FormBuilder, },
        { type: ActionSheetController, },
        { type: Platform, },
        { type: Http, },
        { type: Config, },
        { type: NavParams, },
        { type: AlertController, },
    ];
    AddAdsPage.propDecorators = {
        'dateTime1': [{ type: ViewChild, args: ['dateTime1',] },],
        'dateTime2': [{ type: ViewChild, args: ['dateTime2',] },],
    };
    return AddAdsPage;
}());
//# sourceMappingURL=add-ads.js.map