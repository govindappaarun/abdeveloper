import { Component, ViewChild } from '@angular/core';
import { NavController, ViewController, Platform } from 'ionic-angular';
/*
  Generated class for the ModalAutoComplete page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
export var ModalAutoCompleteComponent = (function () {
    function ModalAutoCompleteComponent(navCtrl, viewCtrl, platform) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.viewCtrl = viewCtrl;
        this.platform = platform;
        this.platform.registerBackButtonAction(function () {
            _this.viewCtrl.dismiss();
        });
    }
    ModalAutoCompleteComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.acService = new google.maps.places.AutocompleteService();
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        };
        setTimeout(function () {
            _this.myInput.setFocus();
        }, 150);
    };
    ModalAutoCompleteComponent.prototype.ionViewDidLoad = function () {
        console.log('Hello ModalAutoCompletePage Page');
    };
    ModalAutoCompleteComponent.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    ModalAutoCompleteComponent.prototype.chooseItem = function (item) {
        console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
    };
    ModalAutoCompleteComponent.prototype.updateSearch = function () {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        var self = this;
        var config = {
            types: ['geocode'],
            input: this.autocomplete.query
        };
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];
            predictions.forEach(function (prediction) {
                self.autocompleteItems.push(prediction);
            });
        });
    };
    ModalAutoCompleteComponent.decorators = [
        { type: Component, args: [{
                    selector: 'page-modal-auto-complete',
                    templateUrl: 'modal-auto-complete.html'
                },] },
    ];
    /** @nocollapse */
    ModalAutoCompleteComponent.ctorParameters = [
        { type: NavController, },
        { type: ViewController, },
        { type: Platform, },
    ];
    ModalAutoCompleteComponent.propDecorators = {
        'myInput': [{ type: ViewChild, args: ['input',] },],
    };
    return ModalAutoCompleteComponent;
}());
//# sourceMappingURL=modal-auto-complete.js.map