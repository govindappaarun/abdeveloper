import { Component, OnInit, ViewChild } from '@angular/core';
import { NavController,ViewController, Platform } from 'ionic-angular';


declare var google: any;


/*
  Generated class for the ModalAutoComplete page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-modal-auto-complete',
  templateUrl: 'modal-auto-complete.html'
})
export class ModalAutoCompleteComponent implements OnInit{
    @ViewChild('input') myInput ;

    autocompleteItems: any;
    autocomplete: any;
    acService:any;
    placesService: any;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, public platform: Platform) {
      this.platform.registerBackButtonAction(() => {
      this.viewCtrl.dismiss()
      
    });
  }
  
  ngOnInit() {
        this.acService = new google.maps.places.AutocompleteService();        
        this.autocompleteItems = [];
        this.autocomplete = {
            query: ''
        }; 
        setTimeout(() => {
            this.myInput.setFocus();
        },150);       
    }

  ionViewDidLoad() {
    console.log('Hello ModalAutoCompletePage Page');
  }

  dismiss() {
        this.viewCtrl.dismiss();
    }

  chooseItem(item: any) {
        console.log('modal > chooseItem > item > ', item);
        this.viewCtrl.dismiss(item);
  }

  updateSearch() {
        console.log('modal > updateSearch');
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }
        let self = this;
        let config = { 
            types:  ['geocode'],
            input: this.autocomplete.query
        }
        this.acService.getPlacePredictions(config, function (predictions, status) {
            console.log('modal > getPlacePredictions > status > ', status);
            self.autocompleteItems = [];            
            predictions.forEach(function (prediction) {              
                self.autocompleteItems.push(prediction);
            });
        });
}

}

