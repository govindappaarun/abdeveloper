import { Component } from '@angular/core';
import { Geolocation } from 'ionic-native';
import { LoadingController, NavController, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Config } from '../../providers/config';
import { ModalAutoCompleteComponent } from '../modal-auto-complete/modal-auto-complete';
import { AddAdsPage } from '../../pages/add-ads/add-ads';
import { AdsdetailPage } from '../../pages/adsdetail/adsdetail';
/*
  Generated class for the Map component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
export var MapComponent = (function () {
    function MapComponent(navController, http, alertCtrl, loadingCtrl, modalCtrl, config) {
        this.navController = navController;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.modalCtrl = modalCtrl;
        this.config = config;
        this.markers = [];
        this.adsmarkers = [];
        this.adsdetails = {};
        this.address = {
            place: '',
            set: false,
        };
        this.location = {
            lat: '',
            lng: ''
        };
        console.log('Hello Map Component');
        this.API_URL = config.API_URL;
    }
    MapComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.createMap();
        this.loadAds();
        this.getCurrentLocation().subscribe(function (location) {
            _this.centerLocation(location);
        });
    };
    MapComponent.prototype.ionViewCanEnter = function () {
        var _this = this;
        this.createMap();
        this.loadAds();
        this.getCurrentLocation().subscribe(function (location) {
            _this.centerLocation(location);
        });
    };
    MapComponent.prototype.launchAd = function (ad) {
        this.navController.push(AdsdetailPage, {
            ad: ad
        });
    };
    MapComponent.prototype.loadAds = function () {
        var _this = this;
        var self = this;
        var headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.get(this.API_URL + '/api/advertisements', { headers: headers })
            .subscribe(function (data) {
            console.log(data.json());
            var res = data.json();
            var ads = res.data;
            console.log(ads.length);
            for (var i = 0; i < ads.length; i++) {
                var lat = ads[i].lat;
                var lng = ads[i].lng;
                console.log('haiii');
                var location_1 = new google.maps.LatLng(lat, lng);
                console.log(location_1);
                //self.createAdsMarker(location);
                var marker = new google.maps.Marker({
                    map: _this.map,
                    icon: 'assets/img/gg.png',
                    position: location_1
                });
                self.adsmarkers.push(marker);
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        var infowindow = new google.maps.InfoWindow;
                        var div = document.createElement('div');
                        div.innerHTML = '<div></br><span class="formatText">Ads: </span>' + ads[i].title
                            + '</br></div>';
                        div.onclick = function () {
                            console.log('info clicked');
                            self.launchAd(ads[i]);
                        };
                        infowindow.setContent(div);
                        infowindow.open(self.map, marker);
                        //google.maps.event.addDomListener(document.getElementById('info'), 'click', removeMarker);
                    };
                })(marker, i));
            }
            /*let alert = this.alertCtrl.create({
               title: 'Successfull',
               subTitle: res.message,
               buttons: ['OK']
            });
            alert.present();*/
        }, function (err) {
            var err = err.json();
            /*let alert = this.alertCtrl.create({
               title: 'Failed',
               subTitle: err.message,
               buttons: ['OK']
            });
            alert.present();*/
        });
    };
    MapComponent.prototype.getCurrentLocation = function () {
        //Create the popup
        var loadingPopup = this.loadingCtrl.create({
            content: 'Your Location...'
        });
        // Show the popup
        loadingPopup.present();
        var options = { timeout: 10000, enableHighAccuracy: true };
        var locationObs = Observable.create(function (observable) {
            Geolocation.getCurrentPosition(options).then(function (position) {
                var lat = position.coords.latitude;
                var lng = position.coords.longitude;
                var location = new google.maps.LatLng(lat, lng);
                observable.next(location);
                loadingPopup.dismiss();
            }, function (err) {
                var latLng = new google.maps.LatLng(-34.9290, 138.6010);
                observable.next(latLng);
                console.log('Geolocation Error :' + err);
                loadingPopup.dismiss();
            });
        });
        return locationObs;
    };
    MapComponent.prototype.createMap = function () {
        var self = this;
        var latLng = new google.maps.LatLng(-34.9290, 138.6010);
        var mapOptions = {
            center: latLng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: true
        };
        var mapEl = document.getElementById('map');
        this.map = new google.maps.Map(mapEl, mapOptions);
        google.maps.event.addListener(this.map, 'click', function (event) {
            console.log('Lattituude : ' + event.latLng.lat() + 'Longitutde:' + event.latLng.lng());
            self.createMapMarker(event.latLng);
            self.location.lat = event.latLng.lat();
            self.location.lng = event.latLng.lng();
            //self.map.panTo(event.latLng);
            //document.getElementById('latlongclicked').value = event.latLng.lat()
            //document.getElementById('lotlongclicked').value =  event.latLng.lng()
        });
    };
    MapComponent.prototype.centerLocation = function (location) {
        var _this = this;
        if (location) {
            this.map.panTo(location);
            this.createMapMarker(location);
        }
        else {
            this.getCurrentLocation().subscribe(function (currentLocation) {
                _this.map.panTo(currentLocation);
                _this.createMapMarker(currentLocation);
            });
        }
    };
    MapComponent.prototype.centerLocationf = function () {
        var _this = this;
        this.getCurrentLocation().subscribe(function (currentLocation) {
            _this.map.panTo(currentLocation);
            _this.createMapMarker(currentLocation);
        });
    };
    MapComponent.prototype.showModal = function () {
        var _this = this;
        //this.reset();
        // show modal|
        var modal = this.modalCtrl.create(ModalAutoCompleteComponent);
        modal.onDidDismiss(function (data) {
            console.log('page > modal dismissed > data > ');
            if (data) {
                console.log('Data description' + data.description);
                console.log('Place ID' + data.place_id);
                _this.address.place = data.description;
                // get details
                //this.getPlaceDetail(data.place_id);
                _this.setPlaceDetails(data.place_id);
            }
        });
        modal.present();
    };
    MapComponent.prototype.setPlaceDetails = function (place_id) {
        var self = this;
        var request = {
            placeId: place_id
        };
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.placesService.getDetails(request, callback);
        function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log('page > getPlaceDetail > place > ', place);
                // set place in map
                //self.map.setCenter(place.geometry.location);
                self.map.panTo(place.geometry.location);
                self.createMapMarker(place.geometry.location);
                // populate
                console.log('page > getPlaceDetail > details >', place);
            }
            else {
                console.log('page > getPlaceDetail > status > error', status);
            }
        }
    };
    MapComponent.prototype.createAdsMarker = function (location) {
        //this.deleteMarkers();
        var marker = new google.maps.Marker({
            map: this.map,
            icon: 'assets/img/gg.png',
            position: location
        });
        this.adsmarkers.push(marker);
    };
    MapComponent.prototype.createMapMarker = function (location) {
        this.deleteMarkers();
        var marker = new google.maps.Marker({
            map: this.map,
            icon: 'assets/img/pin-2.png',
            position: location
        });
        this.markers.push(marker);
    };
    // Sets the map on all markers in the array.
    MapComponent.prototype.setMapOnAll = function (map) {
        for (var i = 0; i < this.markers.length; i++) {
            this.markers[i].setMap(map);
        }
    };
    // Removes the markers from the map, but keeps them in the array.
    MapComponent.prototype.clearMarkers = function () {
        this.setMapOnAll(null);
    };
    // Deletes all markers in the array by removing references to them.
    MapComponent.prototype.deleteMarkers = function () {
        this.clearMarkers();
        this.markers = [];
    };
    MapComponent.prototype.postAds = function () {
        this.navController.push(AddAdsPage, {
            location: this.location
        });
    };
    MapComponent.decorators = [
        { type: Component, args: [{
                    selector: 'map',
                    templateUrl: 'map.html',
                    providers: [Config]
                },] },
    ];
    /** @nocollapse */
    MapComponent.ctorParameters = [
        { type: NavController, },
        { type: Http, },
        { type: AlertController, },
        { type: LoadingController, },
        { type: ModalController, },
        { type: Config, },
    ];
    return MapComponent;
}());
//# sourceMappingURL=map.js.map