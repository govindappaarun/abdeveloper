import { Component, OnInit } from '@angular/core';
import { Geolocation } from 'ionic-native';
import { LoadingController, NavController, ModalController, AlertController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import {Config} from '../../providers/config';
declare var google;

import { ModalAutoCompleteComponent } from '../modal-auto-complete/modal-auto-complete';
import { AddAdsPage } from '../../pages/add-ads/add-ads';
import { AdsdetailPage } from '../../pages/adsdetail/adsdetail';

/*
  Generated class for the Map component.

  See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
  for more info on Angular 2 Components.
*/
@Component({
  selector: 'map',
  templateUrl: 'map.html',
  providers:[Config]
})
export class MapComponent implements OnInit{
  //@ViewChild('map') mapElement: ElementRef;
  public map: any;
  placesService:any;
  markers = [];
  adsmarkers = [];

  public API_URL: string;

  adsdetails:any ={};

  address:any = {
        place: '',
        set: false,
  };
  location:any = {
        lat  : '',
        lng  : ''
  };  
  
  constructor(public navController: NavController, public http: Http,public alertCtrl: AlertController,
   private loadingCtrl: LoadingController, public modalCtrl: ModalController, public config: Config ) {
    console.log('Hello Map Component');
    this.API_URL = config.API_URL;
       
  }
  ngOnInit(){
   this.createMap();
   this.loadAds();
    this.getCurrentLocation().subscribe(location => {
      this.centerLocation(location);
    });
  }

  ionViewCanEnter(){
    this.createMap();
    this.loadAds();
    this.getCurrentLocation().subscribe(location => {
      this.centerLocation(location);
    });
    
  }
  launchAd(ad){
    this.navController.push(AdsdetailPage, {
        ad : ad
      });
  }
  loadAds() {
    var self = this;
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.http.get(this.API_URL + '/api/advertisements', {headers: headers})
            .subscribe(
               data => {
                  console.log(data.json());
                  var res = data.json();
                  var ads = res.data;
                  console.log(ads.length);
                  for(let i=0 ; i < ads.length; i++){
                    let lat = ads[i].lat ; 
                    let lng = ads[i].lng ;
                    console.log('haiii');
                    let location = new google.maps.LatLng(lat, lng);
                    console.log(location);
                    //self.createAdsMarker(location);
                    var marker = new google.maps.Marker({
                        map: this.map,
                        icon: 'assets/img/gg.png',
                        position: location
                    });    
                      self.adsmarkers.push(marker);
                      google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                        var infowindow = new google.maps.InfoWindow;
                        
                        var div = document.createElement('div');
                        div.innerHTML = '<div></br><span class="formatText">Ads: </span>' + ads[i].title 
                                        + '</br></div>';
                        div.onclick = function(){
                          console.log('info clicked');
                          self.launchAd(ads[i]);
                      
                        };
                        infowindow.setContent(div);
                        infowindow.open(self.map, marker);
                        
                        //google.maps.event.addDomListener(document.getElementById('info'), 'click', removeMarker);
                      }
                     })(marker, i));
                  }
                  /*let alert = this.alertCtrl.create({
                     title: 'Successfull',
                     subTitle: res.message,
                     buttons: ['OK']
                  });
                  alert.present();*/
                },
              err => {
                  var err = err.json();
                  /*let alert = this.alertCtrl.create({
                     title: 'Failed',
                     subTitle: err.message,
                     buttons: ['OK']
                  });
                  alert.present();*/
              }
          );
  }
  getCurrentLocation(){
    //Create the popup
    let loadingPopup = this.loadingCtrl.create({
      content: 'Your Location...'
    });

    // Show the popup
    loadingPopup.present();
    let options ={timeout: 10000, enableHighAccuracy :true} ;

    let locationObs = Observable.create(observable => {
      Geolocation.getCurrentPosition(options).then((position) => {
        let lat = position.coords.latitude ; 
        let lng = position.coords.longitude ;

        let location = new google.maps.LatLng(lat, lng);

        observable.next(location);

        loadingPopup.dismiss();
      },
      (err) => {
        let latLng = new google.maps.LatLng(-34.9290, 138.6010);
        observable.next(latLng);
        console.log('Geolocation Error :' + err);
        loadingPopup.dismiss();
      })
    });
    return locationObs;
  }
  createMap (){
    
    var self = this;
    let latLng = new google.maps.LatLng(-34.9290, 138.6010);
 
    let mapOptions = {
      center: latLng,
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true
    }
    let mapEl = document.getElementById('map');
    this.map = new google.maps.Map(mapEl, mapOptions);
    google.maps.event.addListener(this.map,'click',function(event) {
        console.log('Lattituude : '+event.latLng.lat() + 'Longitutde:' + event.latLng.lng());
        self.createMapMarker(event.latLng);
        self.location.lat = event.latLng.lat();
        self.location.lng = event.latLng.lng();
        //self.map.panTo(event.latLng);
                 //document.getElementById('latlongclicked').value = event.latLng.lat()
                 //document.getElementById('lotlongclicked').value =  event.latLng.lng()
      });
    
  }

  centerLocation(location){
    if(location){
      this.map.panTo(location);
      this.createMapMarker(location);
    }else{
      
      this.getCurrentLocation().subscribe(currentLocation => {
            this.map.panTo(currentLocation);
            this.createMapMarker(currentLocation);
      });
    }
  }
  centerLocationf(){
    
      this.getCurrentLocation().subscribe(currentLocation => {
            this.map.panTo(currentLocation);
            this.createMapMarker(currentLocation);
      });
  }

  showModal() {
        //this.reset();
        // show modal|
        let modal = this.modalCtrl.create(ModalAutoCompleteComponent);
        modal.onDidDismiss(data => {
            console.log('page > modal dismissed > data > ');
            if(data){
                console.log('Data description' + data.description);
                console.log('Place ID' + data.place_id);
                this.address.place = data.description;
                // get details
                //this.getPlaceDetail(data.place_id);
                this.setPlaceDetails(data.place_id);
            }              
        })
        modal.present();
  }

  setPlaceDetails(place_id:string):void {
    var self = this;
    var request = {
            placeId: place_id
        };
    this.placesService = new google.maps.places.PlacesService(this.map);
    this.placesService.getDetails(request, callback);
    function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
                console.log('page > getPlaceDetail > place > ', place);
                // set place in map
                //self.map.setCenter(place.geometry.location);
                self.map.panTo(place.geometry.location);
                self.createMapMarker(place.geometry.location);
                // populate
                console.log('page > getPlaceDetail > details >', place );
            }
            else{
                console.log('page > getPlaceDetail > status > error', status);
            }
        }

  }
  private createAdsMarker(location:any):void {
    //this.deleteMarkers();
    var marker = new google.maps.Marker({
            map: this.map,
            icon: 'assets/img/gg.png',
            position: location
        });    
     this.adsmarkers.push(marker);
    }
  private createMapMarker(location:any):void {
    this.deleteMarkers();
    var marker = new google.maps.Marker({
            map: this.map,
            icon: 'assets/img/pin-2.png',
            position: location
        });    
     this.markers.push(marker);
    }
  
      // Sets the map on all markers in the array.
      setMapOnAll(map) {
        for (var i = 0; i < this.markers.length; i++) {
          this.markers[i].setMap(map);
        }
      }

      // Removes the markers from the map, but keeps them in the array.
      clearMarkers() {
        this.setMapOnAll(null);
      }

      // Deletes all markers in the array by removing references to them.
      deleteMarkers() {
        this.clearMarkers();
        this.markers = [];
      }

    postAds(){
     this.navController.push(AddAdsPage, {
        location : this.location
      });
    }
}
