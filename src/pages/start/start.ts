import { Component } from '@angular/core';
import { NavController, MenuController } from 'ionic-angular';

import { LoginPage } from '../login/login';
import { RegistrationPage } from '../registration/registration';

/*
  Generated class for the Start page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-start',
  templateUrl: 'start.html'
})
export class StartPage {

  constructor(public navCtrl: NavController, public menu: MenuController) {}

  ionViewDidLoad() {
    console.log('Hello StartPage Page');
  }

  ionViewDidEnter(){
    this.menu.enable(false);
  }
  ionViewCanLeave(){
    this.menu.enable(true);
  }

  launchLogin(){
    this.navCtrl.push(LoginPage);
  }
  launchRegistration(){
    this.navCtrl.push(RegistrationPage);
  }
}
