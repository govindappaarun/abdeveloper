import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

/*
  Generated class for the Myaccount page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-myaccount',
  templateUrl: 'myaccount.html'
})
export class MyaccountPage {

  constructor(public navCtrl: NavController) {}

  ionViewDidLoad() {
    console.log('Hello MyaccountPage Page');
  }

}
