import { Component} from '@angular/core';
import { NavController, MenuController, Events, AlertController } from 'ionic-angular';
import {Validators, FormBuilder,FormGroup, AbstractControl } from '@angular/forms';
import { Http, Headers } from '@angular/http';
import { HomePage } from '../home/home';
import {Config} from '../../providers/config';

/*
  Generated class for the Registration page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
  providers:[Config]
})
export class RegistrationPage {

  regForm: FormGroup;
  name: AbstractControl;
  email: AbstractControl;
  password: AbstractControl;
  password_match: AbstractControl;

  public API_URL: string;

  constructor(public navCtrl: NavController, 
              private formBuilder: FormBuilder, 
                public menu: MenuController, 
                  public events: Events, public http: Http,
                  public alertCtrl: AlertController, public config: Config ) {
    this.API_URL = config.API_URL;
    this.regForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.minLength(3), Validators.required])],
      email: ['', Validators.compose([Validators.minLength(3),Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'),Validators.required])],
      password: ['', Validators.compose([Validators.minLength(8), Validators.required])],
      password_match: ['', Validators.compose([Validators.minLength(8), Validators.required])]
    });
    this.name = this.regForm.controls['name'];
    this.email = this.regForm.controls['email'];
    this.password = this.regForm.controls['password'];
    this.password_match = this.regForm.controls['password_match'];
  }
   
  ionViewDidLoad() {
    console.log('Hello RegistrationPage Page');
  }
  ionViewDidEnter(){
    this.menu.enable(false);
  }
  ionViewCanLeave(){
    this.menu.enable(true);
  }

  register(){
    console.log(this.regForm.value);
    if(this.regForm.valid) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        var value = this.regForm.value;
          this.http.post(this.API_URL + '/api/register', JSON.stringify(value), {headers: headers})
            .subscribe(
               data => {
                    console.log(data.json());
                    this.events.publish('user:created', value.email);
                    window.localStorage.setItem('user', value.email);
                    this.navCtrl.setRoot(HomePage);
                },
              err => {
                  var err = err.json();
                  let alert = this.alertCtrl.create({
                  title: 'Register Failed',
                  subTitle: err.message,
                  buttons: ['OK']
                });
                alert.present();
              }
          );     
    }
  }
}
