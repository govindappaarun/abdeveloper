import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Adsdetail page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-adsdetail',
  templateUrl: 'adsdetail.html'
})
export class AdsdetailPage {
  ad:any;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.ad = navParams.get('ad');
  }

  ionViewDidLoad() {
    console.log('Hello AdsdetailPage Page');
  }

}
