import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav, Events  } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { StartPage } from '../pages/start/start';
import { HomePage } from '../pages/home/home';
import { AddAdsPage } from '../pages/add-ads/add-ads';
import { ApplicantsPage } from '../pages/applicants/applicants';
import { MyaccountPage } from '../pages/myaccount/myaccount';
import { OrdersPage } from '../pages/orders/orders';
import { Cartpage } from '../pages/cartpage/cartpage';
import { ViewAdsPage } from '../pages/view-ads/view-ads';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage : any ;
  user : any ;
  pages: Array<{title: string, component: any, name: string}>;
  current_page:string = 'HomePage';
  username:string;

  constructor(public platform: Platform, public menu: MenuController, public events: Events) {
    this.initializeApp();
    this.checkPreviousAuthorization();
    // set our app's pages
    this.pages = [
      { title: 'Home Page', component: HomePage, name: 'HomePage'},
      { title: 'My Account', component: MyaccountPage, name: 'MyaccountPage'},
      { title: 'Cart/Basket', component: Cartpage, name: 'Cartpage'},
      { title: 'Orders', component: OrdersPage, name: 'OrdersPage'},
      { title: 'My Ads', component: ViewAdsPage, name: 'ViewAdsPage'},
      { title: 'Registered Users', component: ApplicantsPage, name: 'ApplicantsPage'},
      { title: 'Post Ads', component: AddAdsPage, name: 'AddAdsPage'}
    ];
    this.user = {};
    if(window.localStorage.getItem('user') != null) {
      var userinfo = window.localStorage.getItem('user');
      this.user = JSON.parse(userinfo);
    }

    this.events.subscribe('user:login', (user) => {
      var userinfo = window.localStorage.getItem('user');
      this.user = JSON.parse(userinfo);
    });

    this.events.subscribe('user:signup', (user) => {
      var userinfo = window.localStorage.getItem('user');
      this.user = JSON.parse(userinfo);
    });
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();
      Splashscreen.hide();
    });
  }
  
  checkPreviousAuthorization(): void { 
    if(window.localStorage.getItem('user') === "undefined" || window.localStorage.getItem('user') === null) {
      this.rootPage = StartPage;
      window.localStorage.removeItem('user');
    } else {
      this.rootPage = HomePage;
      var userinfo = window.localStorage.getItem('user');
      this.user = JSON.parse(userinfo);
    }
  }   

  openPage(page) {
    // close the menu when clicking a link from the menu
    this.menu.close();
    // navigate to the new page if it is not the current page
        
    console.log(page.name +' '+ this.current_page);
    if(page.name != this.current_page)
      this.nav.setRoot(page.component);

    this.current_page = page.name;
  }
  
  menuOpened() {
  }

  logout() {
    window.localStorage.removeItem('user');
    this.username = '';
    this.menu.close();
    // navigate to the new page if it is not the current page
    this.nav.setRoot(StartPage);
  }
}
