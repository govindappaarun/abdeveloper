import { Injectable } from '@angular/core';
import { AlertController, LoadingController } from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import { Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import {Config} from './config';

@Injectable()
export class AdvertisementService {
  public API_URL: string;

  constructor(public http: Http, public config: Config,
    public alertCtrl: AlertController,
    private loadingCtrl: LoadingController) {

    this.API_URL = config.API_URL;
  }

  getAll() {

    let data = Observable.create(observable => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.get(this.API_URL + '/api/advertisements', { headers: headers })
        .subscribe(
        data => {
          var res = data.json();
          observable.next(res.data);
          // loadingPopup.dismiss();
        },
        err => {
          var res = err.json();
          observable.next(res.data);
          //loadingPopup.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Advertisements Not found',
            subTitle: res.message,
            buttons: ['OK']
          });
          alert.present();
        }
        );

    });
    return data;
  };

  getAdsByOption(options) {
    //Create the popup
    /* let loadingPopup = this.loadingCtrl.create({
       content: 'Your Location...'
     });
 
     // Show the popup
     loadingPopup.present();*/

    let data = Observable.create(observable => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      this.http.post(this.API_URL + '/api/AdvertisementsBy', options, { headers: headers })
        .subscribe(
        data => {
          var res = data.json();
          observable.next(res.data);
          // loadingPopup.dismiss();
        },
        err => {
          var res = err.json();
          observable.next(res.data);
          //loadingPopup.dismiss();
          let alert = this.alertCtrl.create({
            title: 'Advertisements Not found',
            subTitle: res.message,
            buttons: ['OK']
          });
          alert.present();
        }
        );

    });
    return data;
  };
}
