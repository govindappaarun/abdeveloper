import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';

@Injectable()
export class Config {

  public API_URL: string;

  constructor() {
    this.API_URL = 'http://ec2-35-154-15-127.ap-south-1.compute.amazonaws.com:3000';
    //this.API_URL = 'http://localhost:3000';
  }
}
